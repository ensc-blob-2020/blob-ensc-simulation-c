﻿using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SimulationBlob
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Icon = Properties.Resources.icon1;

            this.CadreParametres = new System.Windows.Forms.GroupBox();
            this.LaunchSim    = new System.Windows.Forms.Button();
            this.Quit         = new System.Windows.Forms.Button();
            this.Titre        = new System.Windows.Forms.Label();
            this.Description  = new System.Windows.Forms.Label();
            this.Parametres   = new System.Windows.Forms.Label();
            this.Taille       = new System.Windows.Forms.NumericUpDown();
            this.Taille_reel  = new System.Windows.Forms.NumericUpDown();
            this.Pas_de_temps = new System.Windows.Forms.NumericUpDown();
            this.NbCases      = new System.Windows.Forms.Label();
            this.Cm           = new System.Windows.Forms.Label();
            this.TickDuration = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Taille)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Taille_reel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pas_de_temps)).BeginInit();
            CadreParametres.SuspendLayout();
            this.SuspendLayout();
            //
            // CadreParametres
            //
            this.CadreParametres.Anchor = AnchorStyles.None;
            CadreParametres.Controls.Add(Parametres);
            CadreParametres.Controls.Add(NbCases);
            CadreParametres.Controls.Add(Cm);
            CadreParametres.Controls.Add(TickDuration);
            CadreParametres.Controls.Add(Taille);
            CadreParametres.Controls.Add(Taille_reel);
            CadreParametres.Controls.Add(Pas_de_temps);
            CadreParametres.Controls.Add(LaunchSim);
            CadreParametres.Controls.Add(Quit);
            CadreParametres.Location = new Point(204,156);
            CadreParametres.Size = new Size(528, 265);
            // 
            // LaunchSim
            // 
            this.LaunchSim.BackColor = Color.ForestGreen;
            this.LaunchSim.Font = new Font("Segoe UI Semibold", 11.25F, FontStyle.Bold, GraphicsUnit.Point);
            this.LaunchSim.Location = new Point(0, 330-156);
            this.LaunchSim.Name = "LaunchSim";
            this.LaunchSim.Size = new Size(257, 91);
            this.LaunchSim.TabIndex = 0;
            this.LaunchSim.Text = "Lancer la simulation";
            this.LaunchSim.UseVisualStyleBackColor = false;
            this.LaunchSim.Click += new System.EventHandler(this.LaunchSim_Click);
            // 
            // Quit
            // 
            this.Quit.BackColor = Color.OrangeRed;
            this.Quit.Font = new Font("Segoe UI Semibold", 11.25F, FontStyle.Bold, GraphicsUnit.Point);
            this.Quit.ForeColor = SystemColors.ControlText;
            this.Quit.Location = new Point(263, 330-156);
            this.Quit.Name = "Quit";
            this.Quit.Size = new Size(265, 91);
            this.Quit.TabIndex = 1;
            this.Quit.Text = "Quitter la simulation";
            this.Quit.UseVisualStyleBackColor = false;
            this.Quit.Click += new System.EventHandler(this.QuitSim_Click);
            // 
            // Titre
            // 
            this.Titre.Anchor = AnchorStyles.Top;
            this.Titre.AutoSize = true;
            this.Titre.Font = new Font("Segoe UI", 24F, FontStyle.Bold, GraphicsUnit.Point);
            this.Titre.Location = new Point(315, 6);
            this.Titre.Name = "Titre";
            this.Titre.Size = new Size(154, 45);
            this.Titre.TabIndex = 5;
            this.Titre.Text = "Simulateur de Blob";
            
            // 
            // Description
            // 
            StringBuilder str = new StringBuilder();
            str.Append("Attention : Ceci est un logiciel de simulation. \n");
            str.Append("Toute expérience doit être réalisée dans un cadre professionnel et encadrée par du personnel qualifié.\n");
            str.Append("L\'entité simulée présente une croissance exponentielle. \n");
            str.Append("Le Physarum polycephalum ne doit pas être pris à la légère.\n");
            str.Append("Ne laissez pas ce logiciel entre les mains d\'un binôme de stagiaires inconscients.\n");
            str.Append("Faites-en bon usage.\n");


            this.Description.Anchor = 
                System.Windows.Forms.AnchorStyles.Top |
                System.Windows.Forms.AnchorStyles.Left | 
                System.Windows.Forms.AnchorStyles.Right;
            
            this.Description.Location = new Point(161, 51);
            this.Description.Name = "Description";
            this.Description.Size = new Size(614, 6*16);
            this.Description.TabIndex = 6;
            this.Description.Text = str.ToString();
            this.Description.TextAlign = ContentAlignment.MiddleCenter;
            this.Description.UseCompatibleTextRendering = true;

            // 
            // Parametres
            // 
            this.Parametres.AutoSize = true;
            this.Parametres.Font = new Font("Segoe UI Semibold", 15.75F, FontStyle.Bold, GraphicsUnit.Point);
            this.Parametres.Location = new Point(121, 0);
            this.Parametres.Name = "Parametres";
            this.Parametres.Size = new Size(277, 30);
            this.Parametres.TabIndex = 7;
            this.Parametres.Text = "Paramètres de la simulation";
            this.Parametres.TextAlign = ContentAlignment.MiddleCenter;

            // 
            // Taille
            // 
            this.Taille.Location = new Point(217, 197-156);
            this.Taille.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.Taille.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Taille.Name = "Taille";
            this.Taille.Size = new Size(295, 23);
            this.Taille.TabIndex = 8;
            this.Taille.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // Taille_reel
            // 
            this.Taille_reel.Location = new Point(421-204, 226-156);
            this.Taille_reel.Name = "Taille_reel";
            this.Taille_reel.Size = new Size(295, 23);
            this.Taille_reel.TabIndex = 9;
            this.Taille_reel.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // Pas_de_temps
            // 
            this.Pas_de_temps.Location = new Point(421-204, 255-156);
            this.Pas_de_temps.Name = "Pas_de_temps";
            this.Pas_de_temps.Size = new Size(295, 23);
            this.Pas_de_temps.TabIndex = 10;
            this.Pas_de_temps.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // NbCases
            // 
            this.NbCases.AutoSize = true;
            this.NbCases.Location = new Point(224-204, 199-156);
            this.NbCases.Name = "NbCases";
            this.NbCases.Size = new Size(158, 15);
            this.NbCases.TabIndex = 11;
            this.NbCases.Text = "Nombre de cases - max = 60";
            // 
            // Cm
            // 
            this.Cm.AutoSize = true;
            this.Cm.Location = new Point(224-204, 228-156);
            this.Cm.Name = "Cm";
            this.Cm.Size = new Size(158, 15);
            this.Cm.TabIndex = 12;
            this.Cm.Text = "Taille réelle (cm) - max = 100";
            // 
            // TickDuration
            // 
            this.TickDuration.AutoSize = true;
            this.TickDuration.Location = new Point(224-204, 257-156);
            this.TickDuration.Name = "TickDuration";
            this.TickDuration.Size = new Size(160, 15);
            this.TickDuration.TabIndex = 13;
            this.TickDuration.Text = "Pas temporel (h) - max = 100";
            
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new Size(937, 484);
            this.Controls.Add(this.Description);
            this.Controls.Add(this.Titre);
            this.Controls.Add(CadreParametres);
            //this.Icon = new Icon("ms-appx:///assets/favicon.ico");
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Blob Marley - Numérique Edition";
            ((System.ComponentModel.ISupportInitialize)(this.Taille)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Taille_reel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pas_de_temps)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox CadreParametres;
        private System.Windows.Forms.Button LaunchSim;
        private System.Windows.Forms.Button Quit;
        private System.Windows.Forms.Label Titre;
        private System.Windows.Forms.Label Description;
        private System.Windows.Forms.Label Parametres;
        private System.Windows.Forms.NumericUpDown Taille;
        private System.Windows.Forms.NumericUpDown Taille_reel;
        private System.Windows.Forms.NumericUpDown Pas_de_temps;
        private System.Windows.Forms.Label NbCases;
        private System.Windows.Forms.Label Cm;
        private System.Windows.Forms.Label TickDuration;
    }
}

