﻿
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SimulationBlob
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            AutoModeLabel = new Label();
            BarreNoire5 = new Label();
            CheckBoxAutoMode = new CheckBox();
            BoutonSauvegarder = new Button();
            BoutonCharger = new Button();
            CadreControle = new GroupBox();
            BoutonQuitter = new Button();
            BoutonParametres = new Button();
            BoutonReinit = new Button();
            BoutonCommencer = new Button();
            BoutonSuivant = new Button();
            BoutonAide = new Button();
            BoutonPrecedent = new Button();
            CadreParametres = new GroupBox();
            LabelHumidite = new Label();
            TxtConcentration = new Label();
            LabelConcentration = new Label();
            TxtPasDeTemps = new Label();
            TxtTempsEcoule = new Label();
            BoutonLumiere = new Button();
            BoutonSel = new Button();
            BoutonNourriture = new Button();
            TitreStimulus = new Label();
            BarreNoire4 = new Label();
            TitreInformations = new Label();
            BoutonBlob = new Button();
            BarreNoire3 = new Label();
            TxtHumidite = new Label();
            TxtTemperature = new Label();
            TitreEnvironnement = new Label();
            BarreNoire2 = new Label();
            TxtQuantite = new Label();
            Quantite = new NumericUpDown();
            TitreNourriture = new Label();
            BarreNoire1 = new Label();
            ColPastille1 = new Panel();
            Cadavre = new Label();
            Lumiere = new Label();
            Blob = new Label();
            Sclerote = new Label();
            ColText1 = new FlowLayoutPanel();
            LabelBlob = new Label();
            LabelSclerote = new Label();
            LabelLumiere = new Label();
            LabelCadavre = new Label();
            TitreLegende = new Label();
            ColPastille2 = new Panel();
            Spores = new Label();
            Sel = new Label();
            Mucus = new Label();
            ColText2 = new Panel();
            LabelSpore = new Label();
            LabelSel = new Label();
            LabelMucus = new Label();
            TxtRatio = new Label();
            TrackbarRatio = new TrackBar();
            TrackbarConcentration = new TrackBar();
            TrackbarTemperature = new TrackBar();
            TrackbarHumidite = new TrackBar();
            LabelRatio = new Label();
            LabelTemperature = new Label();
            NumericTemps = new NumericUpDown();
            LabelTempsEcoule = new Label();
            CadreControle.SuspendLayout();
            CadreParametres.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(Quantite)).BeginInit();
            ColPastille1.SuspendLayout();
            ColText1.SuspendLayout();
            ColPastille2.SuspendLayout();
            ColText2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(TrackbarRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(TrackbarConcentration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(TrackbarTemperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(TrackbarHumidite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(NumericTemps)).BeginInit();
            SuspendLayout();
            // 
            // cadreControle
            // 
            CadreControle.Anchor = AnchorStyles.Bottom;
            CadreControle.BackColor = System.Drawing.SystemColors.HighlightText;
            CadreControle.Controls.Add(BoutonQuitter);
            CadreControle.Controls.Add(BoutonParametres);
            CadreControle.Controls.Add(BoutonReinit);
            CadreControle.Controls.Add(BoutonCommencer);
            CadreControle.Controls.Add(BoutonSuivant);
            CadreControle.Controls.Add(BoutonAide);
            CadreControle.Controls.Add(BoutonPrecedent);
            CadreControle.Controls.Add(BoutonSauvegarder);
            CadreControle.Controls.Add(BoutonCharger);
            CadreControle.FlatStyle = FlatStyle.System;
            CadreControle.Location = new System.Drawing.Point(12, 541);
            CadreControle.Name = "cadreControle";
            CadreControle.Size = new System.Drawing.Size(639, 107);
            CadreControle.TabIndex = 0;
            CadreControle.TabStop = false;
            CadreControle.Text = "Contrôles";
            // 
            // boutonQuitter
            // 
            BoutonQuitter.BackColor = System.Drawing.Color.Red;
            BoutonQuitter.Location = new System.Drawing.Point(521, 54);
            BoutonQuitter.Name = "boutonQuitter";
            BoutonQuitter.Size = new System.Drawing.Size(110, 46);
            BoutonQuitter.TabIndex = 1;
            BoutonQuitter.Text = "QUITTER";
            BoutonQuitter.UseVisualStyleBackColor = false;
            BoutonQuitter.Click += new System.EventHandler(boutonQuitter_Click);
            // 
            // boutonParametres
            // 
            BoutonParametres.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            BoutonParametres.Location = new System.Drawing.Point(473, 19);
            BoutonParametres.Name = "boutonParametres";
            BoutonParametres.Size = new System.Drawing.Size(158, 29);
            BoutonParametres.TabIndex = 3;
            BoutonParametres.Text = "Changer les paramètres";
            BoutonParametres.UseVisualStyleBackColor = false;
            BoutonParametres.Click += new System.EventHandler(boutonParametres_Click);
            // 
            // boutonReinit
            // 
            BoutonReinit.BackColor = System.Drawing.Color.Lime;
            BoutonReinit.Location = new System.Drawing.Point(79, 30);
            BoutonReinit.Name = "boutonReinit";
            BoutonReinit.Size = new System.Drawing.Size(107, 60);
            BoutonReinit.TabIndex = 4;
            BoutonReinit.Text = "Réinitialiser";
            BoutonReinit.UseVisualStyleBackColor = false;
            BoutonReinit.Click += new System.EventHandler(boutonReinit_Click);
            // 
            // boutonCommencer
            // 
            BoutonCommencer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            BoutonCommencer.Location = new System.Drawing.Point(360, 30);
            BoutonCommencer.Name = "boutonCommencer";
            BoutonCommencer.Size = new System.Drawing.Size(107, 60);
            BoutonCommencer.TabIndex = 2;
            BoutonCommencer.Text = "Commencer";
            BoutonCommencer.UseVisualStyleBackColor = false;
            BoutonCommencer.Click += new System.EventHandler(boutonCommencer_Click);
            // 
            // boutonSuivant
            // 
            BoutonSuivant.Location = new System.Drawing.Point(276, 55);
            BoutonSuivant.Name = "boutonSuivant";
            BoutonSuivant.Size = new System.Drawing.Size(78, 35);
            BoutonSuivant.TabIndex = 5;
            BoutonSuivant.Text = "Suivant";
            BoutonSuivant.UseVisualStyleBackColor = true;
            BoutonSuivant.Click += new System.EventHandler(boutonSuivant_Click);
            // 
            // boutonAide
            // 
            BoutonAide.BackColor = System.Drawing.Color.Yellow;
            BoutonAide.Location = new System.Drawing.Point(6, 46);
            BoutonAide.Name = "boutonAide";
            BoutonAide.Size = new System.Drawing.Size(67, 28);
            BoutonAide.TabIndex = 1;
            BoutonAide.Text = "AIDE ?";
            BoutonAide.UseVisualStyleBackColor = false;
            BoutonAide.Click += new System.EventHandler(boutonAide_Click);
            // 
            // boutonPrecedent
            // 
            BoutonPrecedent.Location = new System.Drawing.Point(192, 55);
            BoutonPrecedent.Name = "boutonPrecedent";
            BoutonPrecedent.Size = new System.Drawing.Size(78, 35);
            BoutonPrecedent.TabIndex = 1;
            BoutonPrecedent.Text = "Précédent";
            BoutonPrecedent.UseVisualStyleBackColor = true;
            BoutonPrecedent.Click += new System.EventHandler(boutonPrecedent_Click);
            // 
            // boutonSauvegarder
            // 
            BoutonSauvegarder.Location = new System.Drawing.Point(276, 15);
            BoutonSauvegarder.Name = "boutonSauvegarder";
            BoutonSauvegarder.Size = new System.Drawing.Size(78, 35);
            BoutonSauvegarder.TabIndex = 1;
            BoutonSauvegarder.Text = "Sauvegarder";
            BoutonSauvegarder.Font = new Font(BoutonSauvegarder.Font.FontFamily, 7);
            BoutonSauvegarder.UseVisualStyleBackColor = true;
            BoutonSauvegarder.Click += new System.EventHandler(boutonSauvegarder_Click);
            // 
            // boutonCharger
            // 
            BoutonCharger.Location = new System.Drawing.Point(192, 15);
            BoutonCharger.Name = "boutonCharger";
            BoutonCharger.Size = new System.Drawing.Size(78, 35);
            BoutonCharger.TabIndex = 1;
            BoutonCharger.Text = "Charger";
            BoutonCharger.Font = new Font(BoutonCharger.Font.FontFamily, 7);
            BoutonCharger.UseVisualStyleBackColor = true;
            BoutonCharger.Click += new System.EventHandler(boutonCharger_Click);
            // 
            // cadreParametres
            // 
            CadreParametres.Anchor = ((AnchorStyles)((AnchorStyles.Top | AnchorStyles.Right)));
            CadreParametres.BackColor = System.Drawing.SystemColors.HighlightText;
            CadreParametres.Controls.Add(AutoModeLabel);
            CadreParametres.Controls.Add(BarreNoire5);
            CadreParametres.Controls.Add(CheckBoxAutoMode);
            CadreParametres.Controls.Add(LabelHumidite);
            CadreParametres.Controls.Add(TxtConcentration);
            CadreParametres.Controls.Add(LabelConcentration);
            CadreParametres.Controls.Add(TxtPasDeTemps);
            CadreParametres.Controls.Add(TxtTempsEcoule);
            CadreParametres.Controls.Add(BoutonLumiere);
            CadreParametres.Controls.Add(BoutonSel);
            CadreParametres.Controls.Add(BoutonNourriture);
            CadreParametres.Controls.Add(TitreStimulus);
            CadreParametres.Controls.Add(BarreNoire4);
            CadreParametres.Controls.Add(TitreInformations);
            CadreParametres.Controls.Add(BoutonBlob);
            CadreParametres.Controls.Add(BarreNoire3);
            CadreParametres.Controls.Add(TxtHumidite);
            CadreParametres.Controls.Add(TxtTemperature);
            CadreParametres.Controls.Add(TitreEnvironnement);
            CadreParametres.Controls.Add(BarreNoire2);
            CadreParametres.Controls.Add(TxtQuantite);
            CadreParametres.Controls.Add(Quantite);
            CadreParametres.Controls.Add(TitreNourriture);
            CadreParametres.Controls.Add(BarreNoire1);
            CadreParametres.Controls.Add(ColPastille1);
            CadreParametres.Controls.Add(ColText1);
            CadreParametres.Controls.Add(TitreLegende);
            CadreParametres.Controls.Add(ColPastille2);
            CadreParametres.Controls.Add(ColText2);
            CadreParametres.Controls.Add(TxtRatio);
            CadreParametres.Controls.Add(TrackbarRatio);
            CadreParametres.Controls.Add(TrackbarConcentration);
            CadreParametres.Controls.Add(TrackbarTemperature);
            CadreParametres.Controls.Add(TrackbarHumidite);
            CadreParametres.Controls.Add(LabelRatio);
            CadreParametres.Controls.Add(LabelTemperature);
            CadreParametres.Controls.Add(NumericTemps);
            CadreParametres.Controls.Add(LabelTempsEcoule);
            CadreParametres.FlatStyle = FlatStyle.Popup;
            CadreParametres.Location = new System.Drawing.Point(653, 5);
            CadreParametres.Name = "cadreParametres";
            CadreParametres.Size = new System.Drawing.Size(259, 643);
            CadreParametres.TabIndex = 1;
            CadreParametres.TabStop = false;
            CadreParametres.Text = "Paramètres";
            // 
            // barreNoire5
            // 
            BarreNoire5.BackColor = System.Drawing.SystemColors.ControlText;
            BarreNoire5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            BarreNoire5.Location = new System.Drawing.Point(10, 527);
            BarreNoire5.Name = "labelAutoMode";
            BarreNoire5.Size = new System.Drawing.Size(240, 4);
            BarreNoire5.TabIndex = 12;
            BarreNoire5.UseMnemonic = false;
            // 
            // AutoModeLabel
            // 
            AutoModeLabel.AutoSize = true;
            AutoModeLabel.Location = new System.Drawing.Point(21, 462);
            AutoModeLabel.Name = "AutoModeLabel";
            AutoModeLabel.Size = new System.Drawing.Size(115, 15);
            AutoModeLabel.TabIndex = 13;
            AutoModeLabel.Text = "Mode automatique :";
            AutoModeLabel.Click += new System.EventHandler(this.label2_Click);

            // 
            // labelHumidite
            // 
            LabelHumidite.AutoSize = true;
            LabelHumidite.Location = new System.Drawing.Point(202, 319);
            LabelHumidite.Name = "labelHumidite";
            LabelHumidite.Size = new System.Drawing.Size(26, 15);
            LabelHumidite.TabIndex = 3;
            LabelHumidite.Text = "0 %";
            // 
            // txtConcentration
            // 
            TxtConcentration.AutoSize = true;
            TxtConcentration.Location = new System.Drawing.Point(26, 174);
            TxtConcentration.Name = "txtConcentration";
            TxtConcentration.Size = new System.Drawing.Size(124, 15);
            TxtConcentration.TabIndex = 4;
            TxtConcentration.Text = "Concentration relative";
            // 
            // labelConcentration
            // 
            LabelConcentration.AutoSize = true;
            LabelConcentration.Location = new System.Drawing.Point(202, 174);
            LabelConcentration.Name = "labelConcentration";
            LabelConcentration.Size = new System.Drawing.Size(13, 15);
            LabelConcentration.TabIndex = 5;
            LabelConcentration.Text = "0";
            // 
            // txtPasDeTemps
            // 
            TxtPasDeTemps.AutoSize = true;
            TxtPasDeTemps.Location = new System.Drawing.Point(21, 397);
            TxtPasDeTemps.Name = "txtPasDeTemps";
            TxtPasDeTemps.Size = new System.Drawing.Size(100, 15);
            TxtPasDeTemps.TabIndex = 2;
            TxtPasDeTemps.Text = "Pas temporel (h) :";
            // 
            // txtTempsEcoule
            // 
            TxtTempsEcoule.Anchor = ((AnchorStyles)((AnchorStyles.Top | AnchorStyles.Right)));
            TxtTempsEcoule.AutoSize = true;
            TxtTempsEcoule.Location = new System.Drawing.Point(21, 425);
            TxtTempsEcoule.Name = "txtTempsEcoule";
            TxtTempsEcoule.Size = new System.Drawing.Size(103, 15);
            TxtTempsEcoule.TabIndex = 2;
            TxtTempsEcoule.Text = "Temps écoulé (h) :";
            // 
            // btnLumiere
            // 
            BoutonLumiere.Location = new System.Drawing.Point(129, 594);
            BoutonLumiere.Name = "btnLumiere";
            BoutonLumiere.Size = new System.Drawing.Size(75, 39);
            BoutonLumiere.TabIndex = 4;
            BoutonLumiere.Text = "Lumière";
            BoutonLumiere.UseVisualStyleBackColor = true;
            BoutonLumiere.Click += new System.EventHandler(btnLumiere_Click);
            // 
            // btnSel
            // 
            BoutonSel.Location = new System.Drawing.Point(51, 594);
            BoutonSel.Name = "btnSel";
            BoutonSel.Size = new System.Drawing.Size(75, 39);
            BoutonSel.TabIndex = 5;
            BoutonSel.Text = "Sel";
            BoutonSel.UseVisualStyleBackColor = true;
            BoutonSel.Click += new System.EventHandler(btnSel_Click);
            // 
            // btnNourriture
            // 
            BoutonNourriture.Location = new System.Drawing.Point(129, 555);
            BoutonNourriture.Name = "btnNourriture";
            BoutonNourriture.Size = new System.Drawing.Size(75, 39);
            BoutonNourriture.TabIndex = 3;
            BoutonNourriture.Text = "Nourriture";
            BoutonNourriture.UseVisualStyleBackColor = true;
            BoutonNourriture.Click += new System.EventHandler(btnNourriture_Click);
            // 
            // titreStimulus
            // 
            TitreStimulus.AutoSize = true;
            TitreStimulus.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            TitreStimulus.Location = new System.Drawing.Point(86, 531);
            TitreStimulus.Name = "titreStimulus";
            TitreStimulus.Size = new System.Drawing.Size(87, 21);
            TitreStimulus.TabIndex = 11;
            TitreStimulus.Text = "ELEMENTS";
            // 
            // barreNoire4
            // 
            BarreNoire4.BackColor = System.Drawing.SystemColors.ControlText;
            BarreNoire4.BorderStyle = BorderStyle.Fixed3D;
            BarreNoire4.Location = new System.Drawing.Point(10, 449);
            BarreNoire4.Name = "barreNoire4";
            BarreNoire4.Size = new System.Drawing.Size(240, 4);
            BarreNoire4.TabIndex = 10;
            BarreNoire4.UseMnemonic = false;
            // 
            // titreInformations
            // 
            TitreInformations.AutoSize = true;
            TitreInformations.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            TitreInformations.Location = new System.Drawing.Point(61, 371);
            TitreInformations.Name = "titreInformations";
            TitreInformations.Size = new System.Drawing.Size(129, 21);
            TitreInformations.TabIndex = 10;
            TitreInformations.Text = "INFORMATIONS";
            // 
            // btnBlob
            // 
            BoutonBlob.Location = new System.Drawing.Point(51, 555);
            BoutonBlob.Name = "btnBlob";
            BoutonBlob.Size = new System.Drawing.Size(75, 39);
            BoutonBlob.TabIndex = 2;
            BoutonBlob.Text = "Blob";
            BoutonBlob.UseVisualStyleBackColor = true;
            BoutonBlob.Click += new System.EventHandler(btnBlob_Click);
            // 
            // barreNoire3
            // 
            BarreNoire3.BackColor = System.Drawing.SystemColors.ControlText;
            BarreNoire3.BorderStyle = BorderStyle.Fixed3D;
            BarreNoire3.Location = new System.Drawing.Point(10, 367);
            BarreNoire3.Name = "barreNoire3";
            BarreNoire3.Size = new System.Drawing.Size(240, 4);
            BarreNoire3.TabIndex = 9;
            BarreNoire3.UseMnemonic = false;
            // 
            // txtHumidite
            // 
            TxtHumidite.AutoSize = true;
            TxtHumidite.Location = new System.Drawing.Point(21, 319);
            TxtHumidite.Name = "txtHumidite";
            TxtHumidite.Size = new System.Drawing.Size(78, 15);
            TxtHumidite.TabIndex = 2;
            TxtHumidite.Text = "Humidité (%)";
            // 
            // txtTemperature
            // 
            TxtTemperature.AutoSize = true;
            TxtTemperature.Location = new System.Drawing.Point(21, 271);
            TxtTemperature.Name = "txtTemperature";
            TxtTemperature.Size = new System.Drawing.Size(97, 15);
            TxtTemperature.TabIndex = 2;
            TxtTemperature.Text = "Température (°C)";
            // 
            // titreEnvironnement
            // 
            TitreEnvironnement.AutoSize = true;
            TitreEnvironnement.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            TitreEnvironnement.Location = new System.Drawing.Point(61, 250);
            TitreEnvironnement.Name = "titreEnvironnement";
            TitreEnvironnement.Size = new System.Drawing.Size(143, 21);
            TitreEnvironnement.TabIndex = 3;
            TitreEnvironnement.Text = "ENVIRONNEMENT";
            // 
            // barreNoire2
            // 
            BarreNoire2.BackColor = System.Drawing.SystemColors.ControlText;
            BarreNoire2.BorderStyle = BorderStyle.Fixed3D;
            BarreNoire2.Location = new System.Drawing.Point(10, 246);
            BarreNoire2.Name = "barreNoire2";
            BarreNoire2.Size = new System.Drawing.Size(240, 4);
            BarreNoire2.TabIndex = 3;
            BarreNoire2.UseMnemonic = false;
            // 
            // txtQuantite
            // 
            TxtQuantite.AutoSize = true;
            TxtQuantite.Location = new System.Drawing.Point(67, 222);
            TxtQuantite.Name = "txtQuantite";
            TxtQuantite.Size = new System.Drawing.Size(53, 15);
            TxtQuantite.TabIndex = 8;
            TxtQuantite.Text = "Quantité";
            // 
            // Quantite
            // 
            this.Quantite.Location = new System.Drawing.Point(136, 220);
            this.Quantite.Name = "Quantite";
            this.Quantite.Size = new System.Drawing.Size(55, 23);
            this.Quantite.TabIndex = 2;
            this.Quantite.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // titreNourriture
            // 
            TitreNourriture.AutoSize = true;
            TitreNourriture.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            TitreNourriture.Location = new System.Drawing.Point(82, 106);
            TitreNourriture.Name = "titreNourriture";
            TitreNourriture.Size = new System.Drawing.Size(108, 21);
            TitreNourriture.TabIndex = 2;
            TitreNourriture.Text = "NOURRITURE";
            // 
            // barreNoire1
            // 
            BarreNoire1.BackColor = System.Drawing.SystemColors.ControlText;
            BarreNoire1.BorderStyle = BorderStyle.Fixed3D;
            BarreNoire1.Location = new System.Drawing.Point(10, 102);
            BarreNoire1.Name = "barreNoire1";
            BarreNoire1.Size = new System.Drawing.Size(240, 4);
            BarreNoire1.TabIndex = 2;
            BarreNoire1.UseMnemonic = false;
            // 
            // ColPastille1
            // 
            this.ColPastille1.Controls.Add(this.Cadavre);
            this.ColPastille1.Controls.Add(this.Lumiere);
            this.ColPastille1.Controls.Add(this.Blob);
            this.ColPastille1.Controls.Add(this.Sclerote);
            this.ColPastille1.Location = new System.Drawing.Point(26, 34);
            this.ColPastille1.Name = "ColPastille1";
            this.ColPastille1.Size = new System.Drawing.Size(16, 60);
            this.ColPastille1.TabIndex = 7;
            // 
            // Cadavre
            // 
            this.Cadavre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Cadavre.Location = new System.Drawing.Point(0, 45);
            this.Cadavre.Name = "Cadavre";
            this.Cadavre.Size = new System.Drawing.Size(15, 15);
            this.Cadavre.TabIndex = 0;
            // 
            // Lumiere
            // 
            this.Lumiere.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.Lumiere.Location = new System.Drawing.Point(0, 30);
            this.Lumiere.Name = "Lumiere";
            this.Lumiere.Size = new System.Drawing.Size(15, 15);
            this.Lumiere.TabIndex = 1;
            // 
            // Blob
            // 
            this.Blob.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Blob.Location = new System.Drawing.Point(0, 0);
            this.Blob.Name = "Blob";
            this.Blob.Size = new System.Drawing.Size(15, 15);
            this.Blob.TabIndex = 7;
            // 
            // Sclerote
            // 
            this.Sclerote.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Sclerote.BackColor = System.Drawing.Color.Gold;
            this.Sclerote.Location = new System.Drawing.Point(0, 15);
            this.Sclerote.Name = "Sclerote";
            this.Sclerote.Size = new System.Drawing.Size(15, 15);
            this.Sclerote.TabIndex = 7;
            // 
            // ColText1
            // 
            ColText1.Controls.Add(LabelBlob);
            ColText1.Controls.Add(LabelSclerote);
            ColText1.Controls.Add(LabelLumiere);
            ColText1.Controls.Add(LabelCadavre);
            ColText1.Location = new System.Drawing.Point(48, 34);
            ColText1.Name = "ColText1";
            ColText1.Size = new System.Drawing.Size(94, 63);
            ColText1.TabIndex = 2;
            // 
            // labelBlob
            // 
            LabelBlob.AutoSize = true;
            LabelBlob.Location = new System.Drawing.Point(3, 0);
            LabelBlob.Name = "labelBlob";
            LabelBlob.Size = new System.Drawing.Size(31, 15);
            LabelBlob.TabIndex = 0;
            LabelBlob.Text = "Blob";
            // 
            // labelSclerote
            // 
            LabelSclerote.AutoSize = true;
            LabelSclerote.Location = new System.Drawing.Point(3, 15);
            LabelSclerote.Name = "labelSclerote";
            LabelSclerote.Size = new System.Drawing.Size(54, 15);
            LabelSclerote.TabIndex = 3;
            LabelSclerote.Text = "Sclérotes";
            // 
            // labelLumiere
            // 
            LabelLumiere.AutoSize = true;
            LabelLumiere.Location = new System.Drawing.Point(3, 30);
            LabelLumiere.Name = "labelLumiere";
            LabelLumiere.Size = new System.Drawing.Size(88, 15);
            LabelLumiere.TabIndex = 5;
            LabelLumiere.Text = "Point lumineux";
            // 
            // labelCadavre
            // 
            LabelCadavre.AutoSize = true;
            LabelCadavre.Location = new System.Drawing.Point(3, 45);
            LabelCadavre.Name = "labelCadavre";
            LabelCadavre.Size = new System.Drawing.Size(50, 15);
            LabelCadavre.TabIndex = 7;
            LabelCadavre.Text = "Cadavre";
            // 
            // titreLegende
            // 
            TitreLegende.AutoSize = true;
            TitreLegende.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            TitreLegende.Location = new System.Drawing.Point(97, 10);
            TitreLegende.Name = "titreLegende";
            TitreLegende.Size = new System.Drawing.Size(76, 21);
            TitreLegende.TabIndex = 2;
            TitreLegende.Text = "LEGENDE";
            // 
            // ColPastille2
            // 
            this.ColPastille2.Controls.Add(this.Spores);
            this.ColPastille2.Controls.Add(this.Sel);
            this.ColPastille2.Controls.Add(this.Mucus);
            this.ColPastille2.Location = new System.Drawing.Point(158, 34);
            this.ColPastille2.Name = "ColPastille2";
            this.ColPastille2.Size = new System.Drawing.Size(15, 60);
            this.ColPastille2.TabIndex = 5;
            // 
            // Spores
            // 
            this.Spores.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Spores.Location = new System.Drawing.Point(0, 0);
            this.Spores.Name = "Spores";
            this.Spores.Size = new System.Drawing.Size(15, 15);
            this.Spores.TabIndex = 2;
            // 
            // Sel
            // 
            this.Sel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Sel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Sel.Cursor = System.Windows.Forms.Cursors.Default;
            this.Sel.Location = new System.Drawing.Point(0, 30);
            this.Sel.Name = "Sel";
            this.Sel.Size = new System.Drawing.Size(15, 15);
            this.Sel.TabIndex = 4;
            // 
            // Mucus
            // 
            this.Mucus.BackColor = System.Drawing.Color.Silver;
            this.Mucus.Location = new System.Drawing.Point(0, 15);
            this.Mucus.Name = "Mucus";
            this.Mucus.Size = new System.Drawing.Size(15, 15);
            this.Mucus.TabIndex = 3;
            // 
            // ColText2
            // 
            ColText2.Controls.Add(LabelSpore);
            ColText2.Controls.Add(LabelSel);
            ColText2.Controls.Add(LabelMucus);
            ColText2.Location = new System.Drawing.Point(179, 34);
            ColText2.Name = "ColText2";
            ColText2.Size = new System.Drawing.Size(44, 51);
            ColText2.TabIndex = 7;
            // 
            // labelSpore
            // 
            LabelSpore.AutoSize = true;
            LabelSpore.Location = new System.Drawing.Point(0, 0);
            LabelSpore.Name = "labelSpore";
            LabelSpore.Size = new System.Drawing.Size(42, 15);
            LabelSpore.TabIndex = 2;
            LabelSpore.Text = "Spores";
            // 
            // labelSel
            // 
            LabelSel.AutoSize = true;
            LabelSel.Location = new System.Drawing.Point(-1, 30);
            LabelSel.Name = "labelSel";
            LabelSel.Size = new System.Drawing.Size(22, 15);
            LabelSel.TabIndex = 6;
            LabelSel.Text = "Sel";
            // 
            // labelMucus
            // 
            LabelMucus.AutoSize = true;
            LabelMucus.Location = new System.Drawing.Point(-1, 15);
            LabelMucus.Name = "labelMucus";
            LabelMucus.Size = new System.Drawing.Size(43, 15);
            LabelMucus.TabIndex = 4;
            LabelMucus.Text = "Mucus";
            // 
            // txtRatio
            // 
            TxtRatio.AutoSize = true;
            TxtRatio.Location = new System.Drawing.Point(26, 126);
            TxtRatio.Name = "txtRatio";
            TxtRatio.Size = new System.Drawing.Size(135, 15);
            TxtRatio.TabIndex = 3;
            TxtRatio.Text = "Ratio protéines/glucides";
            // 
            // trackBarRatio
            // 
            TrackbarRatio.Location = new System.Drawing.Point(21, 144);
            TrackbarRatio.Name = "trackBarRatio";
            TrackbarRatio.Size = new System.Drawing.Size(210, 45);
            TrackbarRatio.TabIndex = 2;
            TrackbarRatio.Scroll += new System.EventHandler(trackBarRatio_Scroll);
            // 
            // trackBarConcentration
            // 
            TrackbarConcentration.Location = new System.Drawing.Point(21, 192);
            TrackbarConcentration.Minimum = 1;
            TrackbarConcentration.Name = "trackBarConcentration";
            TrackbarConcentration.Size = new System.Drawing.Size(210, 45);
            TrackbarConcentration.TabIndex = 3;
            TrackbarConcentration.Value = 1;
            TrackbarConcentration.Scroll += new System.EventHandler(trackBarConcentration_Scroll);
            // 
            // trackBarTemperature
            // 
            TrackbarTemperature.Location = new System.Drawing.Point(21, 289);
            TrackbarTemperature.Maximum = 40;
            TrackbarTemperature.Minimum = -10;
            TrackbarTemperature.Name = "trackBarTemperature";
            TrackbarTemperature.Size = new System.Drawing.Size(210, 45);
            TrackbarTemperature.TabIndex = 4;
            TrackbarTemperature.TickFrequency = 5;
            TrackbarTemperature.Scroll += new System.EventHandler(trackBarTemperature_Scroll);
            // 
            // trackBarHumidite
            // 
            TrackbarHumidite.Location = new System.Drawing.Point(26, 337);
            TrackbarHumidite.Maximum = 100;
            TrackbarHumidite.Name = "trackBarHumidite";
            TrackbarHumidite.Size = new System.Drawing.Size(210, 45);
            TrackbarHumidite.TabIndex = 5;
            TrackbarHumidite.TickFrequency = 10;
            TrackbarHumidite.Scroll += new System.EventHandler(trackBarHumidite_Scroll);
            // 
            // labelRatio
            // 
            LabelRatio.AutoSize = true;
            LabelRatio.Location = new System.Drawing.Point(202, 126);
            LabelRatio.Name = "labelRatio";
            LabelRatio.Size = new System.Drawing.Size(13, 15);
            LabelRatio.TabIndex = 4;
            LabelRatio.Text = "0";
            // 
            // labelTemperature
            // 
            LabelTemperature.AutoSize = true;
            LabelTemperature.Location = new System.Drawing.Point(202, 271);
            LabelTemperature.Name = "labelTemperature";
            LabelTemperature.Size = new System.Drawing.Size(29, 15);
            LabelTemperature.TabIndex = 3;
            LabelTemperature.Text = "0 °C";
            // 
            // numericTemps
            // 
            NumericTemps.Location = new System.Drawing.Point(136, 395);
            NumericTemps.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            NumericTemps.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            NumericTemps.Name = "numericTemps";
            NumericTemps.Size = new System.Drawing.Size(100, 23);
            NumericTemps.TabIndex = 2;
            NumericTemps.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelTempsEcoule
            // 
            LabelTempsEcoule.AutoSize = true;
            LabelTempsEcoule.Location = new System.Drawing.Point(202, 425);
            LabelTempsEcoule.Name = "labelTempsEcoule";
            LabelTempsEcoule.Size = new System.Drawing.Size(13, 15);
            LabelTempsEcoule.TabIndex = 2;
            LabelTempsEcoule.Text = "0";
            // 
            // checkBoxAutoMode
            // 
            CheckBoxAutoMode.AutoSize = true;
            CheckBoxAutoMode.Location = new System.Drawing.Point(221, 463);
            CheckBoxAutoMode.Name = "checkBoxAutoMode";
            CheckBoxAutoMode.Size = new System.Drawing.Size(15, 14);
            CheckBoxAutoMode.TabIndex = 2;
            CheckBoxAutoMode.UseVisualStyleBackColor = true;
            CheckBoxAutoMode.CheckedChanged += new System.EventHandler(checkBoxAutoMode_CheckedChanged);

            // 
            // Form2
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            AutoScaleMode = AutoScaleMode.Dpi;
            AutoScroll = true;
            BackColor = System.Drawing.SystemColors.ControlLightLight;
            ClientSize = new System.Drawing.Size(920, 653);
            Controls.Add(CadreParametres);
            Controls.Add(CadreControle);
            Icon = Properties.Resources.icon1;
            Name = "Form2";
            Text = "Blob Marley - Numérique Edition";
            Load += new System.EventHandler(Form2_Load);
            CadreControle.ResumeLayout(false);
            CadreParametres.ResumeLayout(false);
            CadreParametres.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(Quantite)).EndInit();
            ColPastille1.ResumeLayout(false);
            ColText1.ResumeLayout(false);
            ColText1.PerformLayout();
            ColPastille2.ResumeLayout(false);
            ColText2.ResumeLayout(false);
            ColText2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(TrackbarRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(TrackbarConcentration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(TrackbarTemperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(TrackbarHumidite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(NumericTemps)).EndInit();
            ResumeLayout(false);

        }

        #endregion

        private GroupBox CadreControle;
        private Button BoutonAide;
        private Button BoutonPrecedent;
        private Button BoutonSauvegarder;
        private Button BoutonCharger;
        private Button BoutonCommencer;
        private Button BoutonParametres;
        private Button BoutonReinit;
        private Button BoutonSuivant;
        private Button BoutonQuitter;
        private GroupBox CadreParametres;
        private Label LabelBlob;
        private Label LabelSpore;
        private Label LabelSclerote;
        private Label LabelMucus;
        private Label LabelLumiere;
        private Label LabelSel;
        private Label LabelCadavre;
        private FlowLayoutPanel ColText1;
        private Label Blob;
        private Label Sclerote;
        private Label Lumiere;
        private Label Cadavre;
        private Panel ColPastille1;
        private Panel ColText2;
        private Label Spores;
        private Label Mucus;
        private Label Sel;
        private Panel ColPastille2;
        private Label TitreLegende;
        private Label TitreEnvironnement;
        private Label BarreNoire2;
        private Label TxtQuantite;
        private NumericUpDown Quantite;
        private Label TitreNourriture;
        private Label BarreNoire1;
        private Label TxtTemperature;
        private Label TxtHumidite;
        private Label BarreNoire4;
        private Label TitreInformations;
        private Label BarreNoire3;
        private Label TitreStimulus;
        private Button BoutonBlob;
        private Button BoutonNourriture;
        private Button BoutonLumiere;
        private Button BoutonSel;
        private Label TxtTempsEcoule;
        private Label TxtPasDeTemps;

        private TrackBar TrackbarRatio;
        private TrackBar TrackbarConcentration;
        private TrackBar TrackbarTemperature;
        private TrackBar TrackbarHumidite;
        private Label TxtRatio;
        private Label TxtConcentration;
        private Label LabelRatio;
        private Label LabelConcentration;
        private Label LabelTemperature;
        private Label LabelHumidite;
        private Label LabelTempsEcoule;
        private NumericUpDown NumericTemps;
        private CheckBox CheckBoxAutoMode;
        private Label BarreNoire5;
        private Label AutoModeLabel;
    }
}