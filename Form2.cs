﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Threading;
using World;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace SimulationBlob
{
    public partial class Form2 : Form
    {
        public int PasDeTemps;
        public int Taille;
        public int TailleReelle;
        int TailleCase;
        System.ComponentModel.ComponentResourceManager Resources;
        BackgroundWorker BackgroundWorker;
        Graphics Graphics;
        Grid Grille;
        Controle BtnSelect;
        int PaddingX;
        int PaddingY;
        int TICK_DURATION;
        Stack<Grid> PreviousStates;
        public static bool autoMode;

        /* 
            Enumerateur utilisé pour la variable "btnSelect". Correspond aux quatres ajouts possibles à la grille.
        */
        public enum Controle 
        {
            Nourriture,
            Blob,
            Lumiere,
            Sel
        }
        /* Exécuté à la formation de la grille. Initialise la fenêtre comme défini dans Form1.Designer.cs
            Initialise les variables utiles.
        */
        public Form2()
        {
            InitializeComponent();
            
            
            this.Graphics = this.CreateGraphics();
            this.BtnSelect = Controle.Blob;
            this.BoutonBlob.Select();
            this.Resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            autoMode = false;
            
            Blob.BackColor = ColorTranslator.FromHtml(Resources.GetString("blob"));
            Sclerote.BackColor = ColorTranslator.FromHtml(Resources.GetString("sclerote"));
            Lumiere.BackColor = ColorTranslator.FromHtml(Resources.GetString("lumiere"));
            Cadavre.BackColor = ColorTranslator.FromHtml(Resources.GetString("cadavre"));
            Spores.BackColor = ColorTranslator.FromHtml(Resources.GetString("spore"));
            Mucus.BackColor = ColorTranslator.FromHtml(Resources.GetString("mucus"));
            Sel.BackColor = ColorTranslator.FromHtml(Resources.GetString("sel"));
            TICK_DURATION = int.Parse(Resources.GetString("tickDuration"));
            
            this.BackgroundWorker = new BackgroundWorker();
            BackgroundWorker.DoWork += new DoWorkEventHandler(Work);
            BackgroundWorker.WorkerSupportsCancellation = true;
            
            this.MouseClick += onMouseClick;
            this.Paint += DrawGrid;
            this.FormClosing += CloseEvent;
        }
        /*
            Exécuté une fois au chargement de la fenêtre. Crée la grille de simulation.
        */
        private void Form2_Load(object sender, EventArgs e)
        {
            NumericTemps.Value = PasDeTemps;
            PreviousStates = new Stack<Grid>();

            if (File.Exists("./temp.blob"))
            {
                DialogResult result = MessageBox.Show("Il semblerait que la fenêtre ne se soit pas fermée correctement la dernière fois. Voulez-vous récupérer la dernière sauvegarde ?","Récupérer une sauvegarde",MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    Stream tempFile = File.OpenRead("./temp.blob");

                    BinaryFormatter formatter = new BinaryFormatter();
                    Object obj = formatter.Deserialize(tempFile);
                    Grille = (Grid)obj;

                    tempFile.Flush();
                    tempFile.Close();
                    tempFile.Dispose();

                    DrawGrid();
                }
                else
                {
                    Grille = new Grid(Taille,TailleReelle,PasDeTemps);
                    DrawGrid();
                }
            }
            else
            {
                Grille = new Grid(Taille,TailleReelle,PasDeTemps);
                DrawGrid();
            }
        }
        /*
            Adapte la taille de la grille lorsque la fenêtre doit être redessinée.
        */
        private void DrawGrid(object sender, EventArgs e)
        {
            DrawGrid();
        }

        private void DrawGrid()
        {
            CreerGrille();
            RemplirGrille();
        }

        /*
            Sous-programme lancé lorsque le bouton "Commencer" est appuyé.
            Fait évoluer la simulation puis affiche la grille évoluée.
        */
        private void Work(object sender, DoWorkEventArgs e)
        {
            /*Task tickTask = new Task(() => {MessageBox.Show("ticktask");grille.Tick();});
            Task attenteTask = new Task(()=>{MessageBox.Show("attenteTask");Thread.Sleep(tickDuration);});
            while (!backgroundWorker.CancellationPending)
            {
                Task.WaitAll(tickTask,attenteTask);
                RemplirGrille();
            }*/
            while (!BackgroundWorker.CancellationPending)
            {
                Tick();
                Thread.Sleep(TICK_DURATION);
            }

            e.Cancel = true;
        }

        /*
            Exécuté à chaque modification du pas de temps dans la fenêtre de simulation.
            Modifie la variable de l'objet Form2, et modifie la variable de l'objet Grille.
        */
        private void numericTemps_Changed(object sender, EventArgs e)
        {
            PasDeTemps = (int) NumericTemps.Value;
            Grille.TimeTick = (int) NumericTemps.Value;
        }
        
        /*
            A FAIRE : Afficher un MessageBox (ou éventuellement mieux) pour expliquer comment utiliser l'application.
        */
        private void boutonAide_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Cette fonctionnalité n'est pas encore implémentée... Veuillez patienter.","Zut.......");
            
        }

        /*
            A FAIRE : Enregistrer les états précédents (sous quelle forme?) pour pouvoir les réafficher ensuite.
        */
        private void boutonPrecedent_Click(object sender, EventArgs e)
        {
            if (PreviousStates.TryPop(out Grille))
            {
                RemplirGrille();
            }
            else
            {
                MessageBox.Show("Il n'y a pas d'autres grilles en sauvegarde à charger!","Erreur");
                Grille = new Grid(Taille,TailleReelle,PasDeTemps);
                RemplirGrille();
            }
        }

        private void SaveGrid()
        {
            
            Stream stream;
            
            stream = File.OpenWrite("./temp.blob");
            File.SetAttributes("./temp.blob",FileAttributes.Hidden);
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, Grille);
            stream.Flush();
            stream.Close();
            stream.Dispose();
            
        }

        private void SaveGridAs()
        {
            Stream stream;
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "Grille de simulation (*.blob)|*.blob";
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.AddExtension = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if ((stream = saveFileDialog.OpenFile()) != null)
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, Grille);
                    stream.Flush();
                    stream.Close();
                    stream.Dispose();
                }
            }
        }

        private void LoadGrid()
        {
            
            OpenFileDialog openFileDialog = new OpenFileDialog()
            {
                FileName = "Sélectionnez une sauvegarde d'état de simulation",
                Filter = "Grille de simulation (*.blob)|*.blob",
                Title = "Charger le fichier"
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var filePath = openFileDialog.FileName;
                    using (Stream fs = openFileDialog.OpenFile())
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        Object obj = formatter.Deserialize(fs);
                        Grid loadedGrid = (Grid)obj;

                        fs.Flush();
                        fs.Close();
                        fs.Dispose();

                        Grille = loadedGrid;
                        RemplirGrille();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message,"Erreur");
                }
            }
        }

        /*
            Fait évoluer la simulation d'un pas de temps ; modifie la grille.
        */
        private void boutonSuivant_Click(object sender, EventArgs e)
        {
            Tick();
        }

        /*
            Si la simulation ne tourne pas, le bouton la lance ; Si la simulation tourne, le bouton l'arrête.
        */
        private void boutonCommencer_Click(object sender, EventArgs e)
        {
            if (BoutonCommencer.Text == "Commencer" || BoutonCommencer.Text == "Reprendre")
            {
                BoutonSuivant.Enabled = false;
                BoutonCommencer.Text = "Pause";
                
                BackgroundWorker.RunWorkerAsync();
            }
            else
            {
                BoutonCommencer.Text = "Reprendre";
                BoutonSuivant.Enabled = true;
                BackgroundWorker.CancelAsync();
            }
        }

        /*
            Recrée une nouvelle grille, supprime l'ancienne.
            Blanchit les cases
        */
        private void boutonReinit_Click(object sender, EventArgs e)
        {
            Grille = new Grid(Taille,TailleReelle,PasDeTemps);
            RemplirGrille();
        }
        /*
            Demande une confirmation avant de fermer Form2 (et renvoie à Form1)
        */
        private void boutonParametres_Click(object sender, EventArgs e)
        {
            DialogResult confirmation = MessageBox.Show("Attention ! Changer les paramètres réinitialisera la simulation et vous perdrez tous vos changements. Êtes-vous sûr·es de continuer ?","Confirmation",MessageBoxButtons.YesNo);

            if (confirmation == DialogResult.Yes)
            {
                this.Close();
            }
        }
        /*
            Demande une confirmation avant d'arrêter le programme en renvoyant le code 0 (=OK)
        */
        private void boutonSauvegarder_Click(object sender, EventArgs e)
        {
            SaveGridAs();
        }
        private void boutonCharger_Click(object sender, EventArgs e)
        {
            LoadGrid();
        }
        private void boutonQuitter_Click(object sender, EventArgs e)
        {
            DialogResult confirmation = MessageBox.Show("Attention ! En quittant, vous perdrez tous vos changements. Êtes-vous sûr·es de continuer ?","Confirmation",MessageBoxButtons.YesNo);

            if (confirmation == DialogResult.Yes)
            {
                try
                {
                    File.Delete("./temp.blob");
                } catch {}

                Environment.Exit(0);
            }
        }
        /*
            Change la sélection btnSelect en Blob (un clic de souris posera désormais un blob)
        */
        private void btnBlob_Click(object sender, EventArgs e)
        {
            this.BtnSelect = Controle.Blob;
        }
        /*
            Change la sélection btnSelect en Nourriture (un clic de souris posera désormais un point de nourriture)
        */
        private void btnNourriture_Click(object sender, EventArgs e)
        {
            this.BtnSelect = Controle.Nourriture;
        }
        /*
            Change la sélection btnSelect en Lumiere (un clic de souris posera désormais un point de lumière)
        */
        private void btnLumiere_Click(object sender, EventArgs e)
        {
            this.BtnSelect = Controle.Lumiere;
        }
        /*
            Change la sélection btnSelect en Sel (un clic de souris posera désormais un peu de sel)
        */
        private void btnSel_Click(object sender, EventArgs e)
        {
            this.BtnSelect = Controle.Sel;
        }
        /*
            Lorsque le ratio change, modifie le texte à droite
        */
        private void trackBarRatio_Scroll(object sender, EventArgs e)
        {
            double a = this.TrackbarRatio.Value;
            double b = a / 10;
            LabelRatio.Text = b.ToString();
        }
        /*
            Lorsque la concentration change, modifie le texte à droite
        */
        private void trackBarConcentration_Scroll(object sender, EventArgs e)
        {
            double c = TrackbarConcentration.Value;
            double d = c / 10;
            LabelConcentration.Text = d.ToString();
        }
        /*
            Lorsque la température change, informe l'instance de grille de simulation
        */
        private void trackBarTemperature_Scroll(object sender, EventArgs e)
        {
            LabelTemperature.Text = TrackbarTemperature.Value.ToString() + " °C";
            if (Grille != null)
            {
                Grille.Temperature = (int) TrackbarTemperature.Value;
            }
        }
        /*
            Lorsque l'humidité change, modifie le texte à droite
        */
        private void trackBarHumidite_Scroll(object sender, EventArgs e)
        {
            LabelHumidite.Text = TrackbarHumidite.Value.ToString() + " %" ;
        }
        /*
            Dessine à l'écran la grille (uniquement les lignes noires). S'adapte à la taille de l'écran.
        */
        private void CreerGrille()
        {
            const int PADDING_MIN = 12;
            int tailleGrille;

            if (CadreParametres.Location.X < CadreControle.Location.Y)
            {
                tailleGrille = CadreParametres.Location.X - 2 * PADDING_MIN;
                TailleCase = tailleGrille / this.Taille;
                PaddingX = PADDING_MIN;
                PaddingY = CadreControle.Location.Y / 2 - TailleCase*Taille / 2;
            }
            else
            {
                tailleGrille = CadreControle.Location.Y - 2 * PADDING_MIN;
                TailleCase = tailleGrille / this.Taille;
                PaddingY = PADDING_MIN;
                PaddingX = CadreParametres.Location.X / 2 - TailleCase*Taille / 2;
            }

            Pen pen = new Pen(Color.Black, 1);

            //lignes horizontales
            for (int i=0 ; i <= this.Taille ; i++)
            {
                this.Graphics.DrawLine(pen, PaddingX, PaddingY + i*TailleCase, PaddingX + TailleCase*Taille, PaddingY + i*TailleCase);
            }

            //lignes verticales
            for (int i=0 ; i <= this.Taille ; i++)
            {
                this.Graphics.DrawLine(pen, PaddingX + i*TailleCase, PaddingY, PaddingX + i*TailleCase, PaddingY + TailleCase*Taille);
            }
            
            
        }
        /*
            Renvoie un rectangle de coordonnées d'écran correspondant à la case (x,y)
            (le (0,0) est la case en haut à gauche et x correspond à la largeur)
        */
        private Rectangle Case(int x, int y)
        {
            if (x>=Taille || y>=Taille || x < 0 || y < 0)
            {
                throw new ArgumentException("Case(x,y) : la valeur x ou y n'est pas compris dans l'intervalle correct !");
            }

            int xa = x * TailleCase + PaddingX + 1;
            int ya = y * TailleCase + PaddingY + 1;
            int dx = TailleCase - 1;
            int dy = TailleCase - 1; 
            return(new Rectangle(xa,ya,dx,dy));
        }
        /*
            Renvoie la case qui contient les coordonnées de l'écran (x,y) (le (0,0) est en haut à gauche et x correspond à la largeur).
        */
        private (int,int) getCase(int x, int y)
        {
            if (
                x > PaddingX + TailleCase * Taille || 
                x < PaddingX || 
                y > PaddingY + TailleCase * Taille || 
                y < PaddingY)
            {
                return(-1, -1);
            }
            
            int posx = (x - PaddingX) / TailleCase;
            int posy = (y - PaddingY) / TailleCase;

            return(posx, posy);
        }
        /*
            Remplit la grille selon les composants qu'il y a dedans, à partir des couleurs spécifiées dans le fichier Form2.resx
        */
        private void RemplirGrille()
        {
            SolidBrush couleurBlob = new SolidBrush(ColorTranslator.FromHtml(Resources.GetString("blob")));
            SolidBrush couleurSclerote = new SolidBrush(ColorTranslator.FromHtml(Resources.GetString("sclerote")));
            SolidBrush couleurLumiere = new SolidBrush(ColorTranslator.FromHtml(Resources.GetString("lumiere")));
            SolidBrush couleurCadavre = new SolidBrush(ColorTranslator.FromHtml(Resources.GetString("cadavre")));
            SolidBrush couleurSpores = new SolidBrush(ColorTranslator.FromHtml(Resources.GetString("spores")));
            SolidBrush couleurMucus = new SolidBrush(ColorTranslator.FromHtml(Resources.GetString("mucus")));
            SolidBrush couleurSel = new SolidBrush(ColorTranslator.FromHtml(Resources.GetString("sel")));

            SolidBrush couleur;
            for (int i = 0; i < Taille; i++)
            {
                for (int j = 0; j < Taille; j++)
                {
                    couleur = new SolidBrush(Color.White);
                    
                    if(Grille._Grid[i,j].Salt)
                    {
                        couleur = couleurSel;
                    }
                    if (Grille._Grid[i,j].Mucus)
                    {
                        couleur = couleurMucus;
                    }

                    foreach (var agent in Grille._Grid[i,j].Agents)
                    {
                        if (agent.Name == "Food")
                        {
                            Emitter.Food food = (Emitter.Food) agent;
                            double transparence = 1 - 0.6*food.concentration;
                            int blue = (int) (food.ratio * 255.0);
                            couleur = new SolidBrush(Color.FromArgb(255,(int) (transparence * (255 - blue)),0,(int) (transparence * blue)));
                        }
                        if (agent.Name == "Light")
                        {
                            couleur = couleurLumiere;
                        }

                    }
                    if (Grille._Grid[i,j].Veine != null)
                    {
                        couleur = couleurBlob;

                        if (Grille._Grid[i,j].Veine.Blob.Sclerote)
                        {
                            couleur = couleurSclerote;
                        }
                        if (Grille._Grid[i,j].Veine.Blob.Dead)
                        {
                            couleur = couleurCadavre;
                        }
                    }
                    
                    
                    Graphics.FillRectangle(couleur, Case(i,j));
                }
            }

            LabelTempsEcoule.Text = Grille.ElapsedTime.ToString();
        }
        /*
            Lorsque la souris (clic gauche) est cliquée.
            Si le clic est réalisé dans la grille, on considère que c'est pour ajouter un élément dans la simulation.
            Alors on appelle Applique
        */
        private void onMouseClick(object sender, MouseEventArgs e)
        {
            if (
                Grille != null && 
                e.Button == MouseButtons.Left && 
                e.X <= PaddingX + TailleCase * Taille && 
                e.X >= PaddingX && 
                e.Y <= PaddingY + TailleCase * Taille && 
                e.Y >= PaddingY)
            {
                (int,int) pos = getCase(e.X,e.Y);
                Applique(pos.Item1, pos.Item2);
            }
            
        }
        /*
            Modifie la grille de simulation selon l'objet qu'on voulait ajouter.
            Est appellée lorsqu'on clique sur une case de la grille    
        */
        private void Applique(int x, int y)
        {
            World.Case pos = Grille._Grid[x,y];
            switch (BtnSelect)
            {
                case Controle.Blob:
                    // 0 génère un sexe aléatoire (dans [|1 ; 720|])
                    this.Grille.AddBlob(pos,0);
                    break;
                case Controle.Lumiere:
                    this.Grille.AddLight(pos);
                    break;
                case Controle.Nourriture:
                    this.Grille.AddFood(
                        pos,
                        (int) Quantite.Value,
                        (double) ((double)TrackbarConcentration.Value) / 10,
                        (double) ((double)TrackbarRatio.Value) / 10);
                    
                    break;
                case Controle.Sel:
                    this.Grille.AddSalt(pos);
                    break;
            }
            RemplirGrille();
            
        }

        private void Tick()
        {
            SaveGrid();
            Grille.Tick();
            RemplirGrille();

            Stream tempFile = File.OpenRead("./temp.blob");

            BinaryFormatter formatter = new BinaryFormatter();
            Object obj = formatter.Deserialize(tempFile);
            Grid loadedGrid = (Grid)obj;

            tempFile.Flush();
            tempFile.Close();
            tempFile.Dispose();

            PreviousStates.Push(loadedGrid);
            
        }

        private void CloseEvent(object sender, EventArgs e)
        {
            try
            {
                File.Delete("./temp.blob");
            }
            catch (System.IO.IOException err)
            {
                //MessageBox.Show("Impossible de supprimer temp.blob (IOException) "+err.Message,"Debug");
            }
        }
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void checkBoxAutoMode_CheckedChanged(object sender, EventArgs e)
        {
            if(autoMode == false)
            {
                autoMode = true;
            }
            else
            {
                autoMode = false;
            }
        }
    }

}
