# Simulateur de Blob

## Comment l'utiliser ?

### Lancer la simulation

C'est tout simple : Ouvrez le fichier `PROGRAMME` ou `PROGRAMME.bat` situé à la racine du dossier.

Alternativement, vous pouvez aussi lancer le programme `SimulationBlob.exe` situé dans le dossier `bin\Debug\netcoreapp3.1\`

Vous ne pouvez pas le louper !


### Modifier les paramètres

Vous pouvez modifier certains paramètres en dehors de l'application :
* les couleurs des éléments de la grille
* l'espace de temps entre deux étapes de simulation, en millisecondes

Comment ?
Ouvrez le fichier `Form2.resx` avec [**Visual Studio Code**](https://code.visualstudio.com/) (ou mieux : [**VSCodium**](https://vscodium.com/) !), et installez l'extension **ResXpress**.
PAF ! Vous devriez avoir un beau tableau dont vous pouvez éditer les valeurs.

:warning: Ne modifiez pas les clés (la première colonne), ou le programme pourrait s'énerver. Vous êtes prévenus !

## Contexte du projet

Ce projet est la suite d'un premier logiciel de simulation crée en juin 2020 par Simon Audrix et Gabriel Nativel-Fontaine (voir [leur simulation](https://github.com/Gab1i/programmersblob)) dans le cadre de leur stage de deuxième année à l'ENSC, sous la tutelle de Catherine SEMAL. Notre équipe de projet transdisciplinaire a réalisé cette simulation à partir de d'observation détaillée du blob et de son interaction avec l'environnement.

## Contributeurs

| Nom | Contribution |
|-----|--------------|
| Bettina Metraud | Analyse d'un blob réel et rédaction d'un rapport détaillé sur les modifications à apporter |
| Valentine LUCQUIAULT | Analyse d'un blob réel et rédaction d'un rapport détaillé sur les modifications à apporter |
| Hugo CONTARDO | Rédaction d'un paragraphe détaillant le développement de la simulation |
| Paul BERNARD | Programmation de la simulation en C# |