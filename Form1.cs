﻿using System;
using System.Windows.Forms;

namespace SimulationBlob
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
			
        }
        /* Lorsque le bouton "lancer la simulation" est selectionné
        ** Lance la seconde fenêtre Form2 avec les paramètres choisis
        */
        private void LaunchSim_Click(object sender, EventArgs e)
        {
            Form2 Form2 = new Form2();
            Form2.PasDeTemps = (int) Pas_de_temps.Value;
            Form2.Taille = (int) this.Taille.Value;
            Form2.TailleReelle = (int) this.Taille_reel.Value;
            Form2.ShowDialog();
        }
        /* Lorsque le bouton "Quitter" est selectionné
        ** Ferme la fenêtre et met fin au programme.
        */
        private void QuitSim_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
