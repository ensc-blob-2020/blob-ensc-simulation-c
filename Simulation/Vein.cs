using System.Collections.Generic; // pour les listes
using World;
using System;

namespace Blob {
    [Serializable]
    class Vein
    {
        // Blob qui contient la veine.
        public Blob Blob {get; set;}
        public Case Position {get; set;}
        // Veine parent. null si c'est la veine "racine" (initiale) du blob.
        public Vein Parent {get; set;}
        // Veines engendrées par la veine.
        public List<Vein> Enfants {get; set;}
        
        public Vein(Blob blob, Case position, Vein parent)
        {
            this.Blob = blob;
            this.Position = position;
            this.Parent = parent;
            this.Enfants = new List<Vein>();

            if (parent != null)
            {
                parent.Enfants.Add(this);
            }
        }

        public override string ToString()
        {
            return Blob.ToString();
        }
        
        /*
            Renvoie true si la veine est une extrémité
        */
        public bool isExtremity()
        {
            return(this.Enfants.Count == 0 || (this.Parent == null && this.Enfants.Count == 1));
        }

        /*
            Tue la veine en supprimant sa référence dans son parent et ses enfants
        */
        public void Kill()
        {
            if (this.Parent != null)
            {
                this.Parent.Enfants.Remove(this);
            }

            foreach (var enfant in this.Enfants)
            {
                enfant.Parent = null;
            }

        }

    }

}