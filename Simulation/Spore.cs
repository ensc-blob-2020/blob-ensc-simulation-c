using System;
using System.Collections.Generic;
using World;

namespace Blob {
    [Serializable]
    class Spore {
        // Entier compris entre 1 et 720. Correspond au sexe du spore
        public int Sexe {get; set;} 
        // Case où se trouve le spore
        public Case Position {get; set;} 

        public Spore(int sexe, Case position) {
            this.Sexe = sexe;
            this.Position = position;
        }
        public override string ToString()
        {
            return (String.Format("Spore :\n\tSexe : {0}",Sexe));
        }
        /*
            Déplacement de la spore d'une case autour de sa position initiale.
            Retourne vrai si elle peut se reproduire avec une autre spore
        */
        public bool Move(List<Case> neighbours) {

            Random random = new Random();
            Case choix = neighbours[random.Next(neighbours.Count - 1)]; // Choisit une position au hasard parmi les voisins.

            choix.AddSpore(this);
            this.Position.RemoveSpore(this);
            this.Position = choix;

            return(this.Position.TimeToken());
            

        }
    }
}