using World;
using Emitter;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blob
{
    [Serializable]
    class Blob
    {
        // Identifiant du blob, inutile pour la simulation mais permet de l'identifier si la fenêtre de simulation le permet
        int id { get; set; }
        // Référence à la grille de simulation dans laquelle il est
        Grid World {get; set;}
        // Actuellement inutilisé. Correspond à la quantité de sel que le blob contient (plus il en a, moins il craint le sel)
        double Learning {get; set;}
        // Faim
        int Hunger {get; set;}
        // Entier entre 1 et 720. Correspond au "sexe" du physarum polycephalum
        int Sexe {get; set;}
        // Le blob se divise en veines 
        List<Vein> Veines {get; set;}
        // Toutes les cases qui contiennent les cases
        List<Case> Cases {get; set;}
        // Nombre maximal de veines que le blob peut posséder. 5 au début, augmente lorsque le blob mange des protéines.
        // ?? Ne diminue jamais
        int Max {get; set;}
        // Âge du blob, en nombre de ticks. Lorsqu'il est très vieux (+ de 720 ticks), ses capacités de développement sont plus limitées
        public int Age {get; set;}
        // Représente l'état de moisissure du blob. Réel entre 0 et 1
        double Moisture {get; set;}
        // Quantité de glucides ingérées par le blob
        double Glucides {get; set;}
        // Quantité de protéines ingérées par le blob
        double Proteines {get; set;}
        // Niveau de satiété du blob. Lorsque nutritionState est négatif, le blob meurt, se dessèche ou sporule.
        int NutritionState {get; set;}
        // Vrai si le blob est sous forme de sclérote
        public bool Sclerote {get; set;}
        // Vrai si le blob est mort
        public bool Dead {get; set;}
        // Temps coincé dans du mucus
        public int TimeInMucus {get; set;}

        // Ajout d'un mode automatique
        public bool AutoMode()
        {
            return SimulationBlob.Form2.autoMode;
            
        }

        public Blob(
            Grid _world,
            int id_blob,
            Case pos1,
            Case pos2,
            int _sexe)
        {

            id = id_blob;
            World = _world;
            Learning = 1;
            Hunger = 40;
            
            if (_sexe==0)
            {
                Random r = new Random();
                Sexe = r.Next(1,721);
            }
            else
            {
                Sexe = _sexe;
            }

            Veines = new List<Vein>();
            Cases = new List<Case>();
            Max = 5;
            Age = 1;
            Moisture = 0.6;
            Glucides = 5;
            Proteines = 5;
            NutritionState = 100;

            Sclerote = false;
            Dead = false;
            TimeInMucus = 0;

            Vein v1 = CreateVein(pos1, null);
            Vein v2 = CreateVein(pos2, v1);

        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.AppendFormat("Blob #{0}",id);
            if (Dead)
            {
                str.AppendFormat(" (mort)");
                return(str.ToString()); 
            }
            if (Sclerote)
            {
                str.AppendFormat(" (sclérote) :\n");
                str.AppendFormat("\tÂge : {0}\n",Age);
                str.AppendFormat("\tSexe : {0}\n",Sexe);
                return(str.ToString());
            }
            str.AppendFormat("\tÂge : {0}\n",Age);
            str.AppendFormat("\tSexe : {0}\n",Sexe);
            str.AppendFormat("\tQuantité de sel : {0}\n",Learning);
            str.AppendFormat("\tFaim : {0}\n",Hunger);
            str.AppendFormat("\tTaille maximale : {0}\n",Max);
            str.AppendFormat("\tEtat de moisissure : {0}\n",Moisture);
            str.AppendFormat("\tTaux de glucides : {0}\n",Glucides);
            str.AppendFormat("\tTaux de protéines : {0}\n",Proteines);
            return(str.ToString());
        }
        /*
            Réhydrate le blob, il sort du mode sclérote
        */
        public void Rehydrate()
        {
            Sclerote = false;
        }
        /*
            Retourne vrai si le blob est complètement entouré par du mucus
        */
        public bool StuckInMucus()
        {
            foreach(Vein veine in Veines)
            {
                foreach (Case pos in veine.Position.Voisins)
                {
                    if (!Cases.Contains(pos) && pos.Mucus)
                    {
                        return(false);
                    }
                }
            }
            return(true);
        }
        /*
            Rend le blob plus moisi
        */
        public void Moisturize(double moisture)
        {
            double factor = 0.01;
            moisture = Math.Min(1, Math.Max(0, moisture + (moisture * factor)));
        }
        /*
            Dessèche le blob
        */
        public void Dessication()
        {
            NutritionState = 100;
            Sclerote = true;
        }
        /*
            Sporule le blob. Remplaces toutes les cases par des spores et renvoies les spores d'un même blob
        */
        public List<Spore> Sporulation()
        {
            List<Spore> liste = new List<Spore>();
            foreach (Case pos in Cases)
            {
                pos.RemoveVein();
                Spore s = new Spore(Sexe, pos);
                pos.AddSpore(s);
                liste.Add(s);
            }

            Veines = new List<Vein>();
            Cases = new List<Case>();
            Dead = true;

            return(liste);

        }
        /*
            Renvoie l'état que le blob doit prendre en fonction de son état de satiété
        */
        public string FoodState()
        {
            if (NutritionState <= 0)
            {
                return("faim");
            }
            else
            {
                if (Proteines / Glucides > 7)
                {
                    return("split");
                }
                else {
                    if (Glucides / Proteines > 7)
                    {
                        return("mort");
                    }
                    else {
                        return("vit");
                    }
                }
            }
        }
        /*
            Renvoie l'état que le blob doit prendre en fonction de son état de moisissure
        */
        public int MoistureState()
        {
            if (Moisture < 0.5)
            {
                return(-1);
            }
            if (0.5 <= Moisture && Moisture <= 0.8)
            {
                return(0);
            }
            else {
                return(1);
            }
        }
        /*
            Mets à jour l'âge du blob
        */
        public void Aging()
        {
            Age++;
        }
        /*
            Appellée à chaque tick, rend le blob plus affamé et lui fait oublier le sel.
        */
        public void Starve()
        {
            if (!AutoMode()) NutritionState--;
            Proteines -= 0.05;
            Max = Math.Max(5,(int)Math.Round(Proteines+0.5));
            Glucides -= 0.05;
            Learning = Math.Max(1, Learning - 0.1);
        }
        /*
            Si le blob a faim et qu'une de ses veines est sur une case contenant de la nourriture,
            le blob se nourrit. 
        */
        public bool Feed()
        {
            Emitter.Food nourriture;
            if (Hunger <= 40)
            {
                foreach(Case pos in Cases)
                {
                    if (pos.IsIn("Food") && pos.Veine != null)
                    {
                        foreach(Emitter.Emitter agent in pos.Agents)
                        {
                            if (agent.Name == "Food") {
                                nourriture = agent as Food;
                                double proteine = 0;
                                double glucide = 0;
                                if (!AutoMode())
                                {
                                    proteine = nourriture.Eat();
                                    glucide = 1 - proteine;
                                    NutritionState += 10;
                                }
                                Proteines += proteine;
                                Glucides += glucide;
                                Max = (int) Math.Round(Proteines);

                            }
                        }

                        
                    }
                }
            }
            return(false);
        }
        /*
            Détruit la veine la moins intéressnte (déterminé par FindWorst) et y dépose du mucus
        */
        void KillVein()
        {
            Case worst = FindWorst();
            if (worst == null)
            {
                return;
            }

            Cases.Remove(worst);
            Veines.Remove(worst.Veine);
            worst.Veine.Kill();
            worst.RemoveVein();
            worst.Mucus = true;

        }
        /*
            Ajoute une veine là où c'est le plus intéressant.
            ??? Pour le moment, neighbourhood est toujours égal à false.
        */
        string AddVein(bool neighbourhood)
        {
            if (Veines.Count < Max)
            {
                if (neighbourhood)
                {
                    /*cases, parent = findBestNeighbour();
                    CreateVein(cases,parent);*/
                    // Inutile pour le moment

                }
                else
                {
                    Case pos2 = FindBestVein();
                    if (pos2 != null && LetsMakeLive(pos2) == "merged")
                    {
                        return("merged");
                    }
                }
            }
            return("");
        }
        /*
            Tue une veine. En ajoute une ou deux en plus.
        */
        public void MoveAndGrow()
        {
            KillVein();
            if (AddVein(false) != "merged")
            {
                AddVein(false);
            }
        }
        /*
            Si le blob est sur une case salée, il apprend à aimer le sel
        */
        public void Learn()
        {
            foreach( Case pos in Cases)
            {
                if(pos.Salt)
                {
                    Learning += 0.2;
                }
            }
        }
        /*
            tue le blob
        */
        public void Die()
        {
            Dead = true;
        }
        /*
            Fusionne deux blobs (this et blob)
        */
        void Merge(Blob blob)
        {
            this.World.RemoveBlob(blob);
            this.Max += blob.Max;

            foreach(Vein veine in blob.Veines)
            {
                veine.Blob = this;
                this.Veines.Add(veine);
            }
            foreach(Case pos in blob.Cases)
            {
                this.Cases.Add(pos);
            }
        }
        /*
            Retourne true si il existe une case sans veine dans le voisinage d'une case
        */
        bool AvailableWithMucus(Case pos)
        {
            List<Case> voisinage = pos.Voisins;
            foreach(Case voisin in voisinage)
            {
                if (voisin.Veine==null)
                {
                    return(true);
                }
            }
            return(false);
        }
        /*
            Retourne true si il existe une case sans veine et sans mucus dans le voisinage d'une case
        */
        bool HasAvailableNeighbours(Case pos)
        {
            List<Case> voisinage = pos.Voisins;
            foreach(Case pos2 in voisinage)
            {
                if (pos2.Veine == null && !pos2.Mucus)
                {
                    return(true);
                }
            }
            return(false);
        }
        /*
            Détermine la case environnante la plus intéressante où se développer autour du blob 
        */
        Case FindBestVein()
        {
            List<Case> candidats = new List<Case>();
            double maxDetected = -10;

            List<Case> availables = new List<Case>();
            List<Case> lastChoice = new List<Case>();

            foreach(Case pos in Cases)
            {
                if (AvailableWithMucus(pos))
                {
                    lastChoice.Add(pos);
                }
                if (HasAvailableNeighbours(pos))
                {
                    availables.Add(pos);

                    foreach(Emitter.Emitter e in World.Emitters)
                    {
                        int distance = TchebychevDistance(pos,e.Position);

                        if (distance <= e.Distance)
                        {
                            double powerDetected = e.Power + (e.Decay * distance);
                            if (powerDetected > maxDetected)
                            {
                                maxDetected = powerDetected;
                                candidats = new List<Case>() {pos};
                            }
                            else {
                                if (powerDetected == maxDetected)
                                {
                                    candidats.Add(pos);
                                }
                            }
                        }
                    }
                }

            }

            Random r = new Random();

            if (candidats.Count == 0)
            {
                if (availables.Count == 0)
                {
                    if (lastChoice.Count == 0) return null;
                    
                    return(lastChoice[r.Next(0,lastChoice.Count)]);
                }
                else
                {
                    return(availables[r.Next(0,availables.Count)]);
                }
            }
            if (candidats.Count == 1)
            {
                return(candidats[0]);
            }
            if (candidats.Count == Veines.Count)
            {
                return(candidats[candidats.Count - 1]);
            }

            return(candidats[r.Next(0,candidats.Count)]);

        }
        /*
            Détermine une case parmi les cases les moins intéressantes pour le blob en fonction de présence de nourriture et de valeur de champ.
        */
        Case FindWorst()
        {
            List<Case> pire = new List<Case>();

            foreach(Case c in Cases)
            {   
                // Si le blob est coincé dans du mucus, il cherche de plus en plus loin un endroit où s'enfuir.
                // Une valeur artificielle est ajoutée dans une case sans mucus, pour qu'il y fuit le mucus.
                List<Case> voisinage = this.World.Voisinage(c, TimeInMucus);
                int v = 0;

                foreach (Case pos in voisinage)
                {
                    if (!pos.Mucus)
                    {
                        v += 1;
                    }
                }

                c.MergeField(v);

                if (c.Veine.isExtremity() && !c.IsIn("Food"))
                {
                    if (pire.Count == 0)
                    {
                        pire.Add(c);
                    }
                    else {
                        if (c.ValeurDeChamp < pire[0].ValeurDeChamp)
                        {
                            pire = new List<Case>() { c };
                        }
                        else {
                            if (c.ValeurDeChamp == pire[0].ValeurDeChamp)
                            {
                                pire.Add(c);
                            }
                        }
                    }
                }

            }

            if (pire.Count == 0)
            {
                return(null);
            }
            
            Random r = new Random();
            return(pire[r.Next(0,pire.Count)]);
        }
        /*
            Crée une veine produite à partir de la veine parent à une position donnée, et la retourne si la veine est crée
        */
        Vein CreateVein(Case pos, Vein parent)
        {
            if (pos.Veine != null && pos.Veine.Blob != this && parent != null)
            {
                pos.Veine.Parent = parent;
                parent.Enfants.Add(pos.Veine);
                pos.Veine.Blob.Merge(this);
                return(null);
            }
            else
            {
                Vein v = new Vein(this,pos,parent);
                Veines.Add(v);
                Cases.Add(pos);

                pos.Veine = v;
                return(v);

            }
        }
        /*
            Crée une veine au meilleur endroit, si possible pas sur une case avec mucus
        */
        string LetsMakeLive(Case pos)
        {
            List<Case> voisinage = pos.Voisins;
            List<Case> available = new List<Case>();
            List<Case> withMucus = new List<Case>();

            foreach (Case c in voisinage)
            {
                if ((c.Veine == null || c.Veine.Blob != this) && !c.Mucus)
                {
                    available.Add(c);
                } 
                if (c.Veine == null || c.Veine.Blob != this)
                {
                    withMucus.Add(c);
                }
            }

            Random r = new Random();
            if (available.Count == 0)
            {
                int nb = r.Next(0,withMucus.Count);
                if (CreateVein(withMucus[nb], pos.Veine) == null)
                {
                    return("merged");
                }

            }
            else
            {
                int nb = r.Next(0, available.Count);
                if (CreateVein(available[nb], pos.Veine) == null)
                {
                    return("merged");
                }
            }
            return("");
        }
        /*
            Retourne la distance de Tchebychev entre deux points. Voir le rapport de Simon Audrix et Gabriel Nativel-Fontaine
        */
        int TchebychevDistance(Case posA, Case posB)
        {
            return(Math.Max(
                Math.Abs(
                    posA.X - posB.X
                ),
                Math.Abs(
                    posA.Y - posB.Y
                )
            ));
        }

    }
}