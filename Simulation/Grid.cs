using System;
using System.Windows.Forms;
using System.Collections.Generic;
using Blob;
using Emitter;

namespace World {
    [Serializable]
    class Grid {
        // Liste de toutes les spores présentes dans la grille
        List<Spore> Spores;
        // Hauteur de la grille en nombre de cases (25 par défaut)
        int Height;
        // Largeur de la grille en nombre de cases (25 par défaut)
        int Width;
        // Tableau bidimensionnel contenant toutes les cases. C'est la grille
        public Case[,] _Grid {get; set;}
        // Taille réelle de la grille, en unité arbitraire
        int TailleReelle;
        // Pas de temps (temps par tick)
        public int TimeTick {get; set;}
        // Temps écoulé, en nombre de ticks
        public int ElapsedTime {get; set;}
        // Bah température hein
        public int Temperature {get; set;}
        // Indice de moisissure de la grille.
        int Moisture;
        // Liste de tous les émetteurs de la grille
        public List<Emitter.Emitter> Emitters;
        // Liste de tous les blobs de la grille
        List<Blob.Blob> Blobs;

        public Grid(int nbCases, int tailleRelle, int tick)
        {
            this.Spores = new List<Spore>();
            this.Height = nbCases;
            this.Width = nbCases;
            this._Grid = new Case[this.Height,this.Width];

            for (int x = 0 ; x < this.Height ; x++)
            {
                for (int y = 0 ; y < this.Width ; y++)
                {
                    this._Grid[x,y] = new Case(x,y,this);
                    
                }
            }
            for (int x = 0 ; x < this.Height ; x++)
            {
                for (int y = 0 ; y < this.Width ; y++)
                {
                    this._Grid[x,y].Voisins = this.Voisinage(this._Grid[x,y], 1);
                }
            }

            this.TailleReelle = tailleRelle;
            this.TimeTick = tick;
            this.ElapsedTime = 0;
            this.Temperature = 27;
            this.Moisture = 0;
            this.Emitters = new List<Emitter.Emitter>();
            this.Blobs = new List<Blob.Blob>();


        }
        /*
            Inutilisé. A quoi correspond valeur ?
        */
        public void SetMoisture(int valeur)
        {
            if (0 <= valeur && valeur <= 46)
            {
                this.Moisture = -1;
            }
            else
            {
                if (47 <= valeur && valeur <= 80)
                {
                    this.Moisture = 0;
                }
                else
                {
                    this.Moisture = 1;
                }
            }
        }
        /*
            Liste de toutes les cases situées à une distance n d'une autre case. (forme un carré de taille 2n+1)
        */
        public List<Case> Voisinage(Case pos, int n)
        {
            List<Case> candidats = new List<Case>();

            // Barre haut
            if (pos.X - n >= 0){
                for (int k = Math.Max(0 , pos.Y - n) ; k < Math.Min(this.Width, pos.Y + n) ; k++)
                {
                    candidats.Add(this._Grid[pos.X - n, k]);
                }
            }
            // Barre bas
            if (pos.X + n < this.Height){
                for (int k = Math.Max(0 , pos.Y - n) ; k < Math.Min(this.Width, pos.Y + n) ; k++)
                {
                    candidats.Add(this._Grid[pos.X + n, k]);
                }
            }
            // Barre gauche
            if (pos.Y - n >= 0){
                for (int k = Math.Max(0 , pos.X - n) ; k < Math.Min(this.Height, pos.X + n) ; k++)
                {
                    candidats.Add(this._Grid[k, pos.Y - n]);
                }
            }
            // Barre droite
            if (pos.Y + n < this.Width){
                for (int k = Math.Max(0 , pos.X - n) ; k < Math.Min(this.Height, pos.X + n) ; k++)
                {
                    candidats.Add(this._Grid[k, pos.Y + n]);
                }
            }
            

            return(candidats);
        }
        /*
            Ajoute un émetteur dans la grille
        */
        public void AddAgent(Emitter.Emitter agent, Case pos)
        {
            pos.AddAgent(agent);
            this.Emitters.Add(agent);
        }
        /*
            Supprime un émetteur dans la grille
        */
        public void RemoveAgent(Emitter.Emitter agent, Case pos)
        {
            this.Emitters.Remove(agent);
            pos.Agents.Remove(agent);
        }
        /*
            Calcule et actualise la valeur de champ de chaque case de la grille
        */
        public void ComputeField()
        {
            this.ResetFields();

            foreach(Emitter.Emitter emetteur in this.Emitters)
            {
                double d = emetteur.Decay;

                for (int i = 0 ; i < Math.Abs(d) ; i++)
                {
                    List<Case> voisinage = this.Voisinage(emetteur.Position , i);

                    foreach(Case pos in voisinage)
                    {
                        if(emetteur.Name == "Mucus")
                        {
                            pos.ValeurDeChamp = emetteur.Power + ((emetteur.Power / emetteur.Decay) * i);

                        }
                        else
                        {
                            pos.MergeField(emetteur.Power + ((emetteur.Power / emetteur.Decay) * i));
                        }
                    }
                }
                
            }
        }
        // Réinitialise les valeurs de champ de chaque case
        public void ResetFields()
        {
            foreach(Case pos in this._Grid)
            {
                pos.ResetField();
            }
        }
        /*
            Met à jour l'état du blob en fonction de son état
        */
        public void UpdateBlobState(Blob.Blob blob)
        {
            if (this.Temperature < 5 || this.Temperature >= 40)
            {
                blob.Die();
            }
            else
            {
                
                switch (blob.MoistureState())
                {
                    case -1:
                        switch (blob.FoodState())
                        {
                            case "faim":
                                blob.Dessication();
                                break;
                            case "split":
                                break;
                            case "mort":
                                blob.Die();
                                break;
                            default:
                                break;
                        }
                        break;
                    case 1:
                        switch (blob.FoodState())
                        {
                            case "faim":
                                this.Spores.AddRange(blob.Sporulation());
                                this.Blobs.Remove(blob);
                                break;
                            case "split":
                                break;
                            case "mort":
                                blob.Die();
                                break;
                            default:
                                blob.Rehydrate();
                                break;

                        }
                        break;
                    default:
                        switch (blob.FoodState())
                        {
                            case "faim":
                                blob.Die();
                                break;
                            case "split":
                                break;
                            case "mort":
                                blob.Die();
                                break;
                            default:
                                blob.Rehydrate();
                                break;

                        }
                        break;
                } 
            }
        }
        /*
            Opère un tick de simulation. Appelé à chaque appel
        */
        public void Tick()
        {
            int nbTick = (int) Math.Round((double) (this.Height * this.TimeTick) / this.TailleReelle);
            this.ElapsedTime += this.TimeTick;

            for (int i = 0 ; i < nbTick ; i++)
            {
                this.ComputeField();

                for (int b = 0 ; b < this.Blobs.Count ; b++)
                {
                    
                    if (Blobs[b].StuckInMucus())
                    {
                        Blobs[b].TimeInMucus += 1;
                    }
                    else
                    {
                        Blobs[b].TimeInMucus = 1;
                    }

                    int direction = this.Moisture == Blobs[b].MoistureState() ? 0 : (this.Moisture < Blobs[b].MoistureState() ? -1 : 1);
                    Blobs[b].Moisturize(direction);
                    Blobs[b].Aging();

                    Random r = new Random();
                    
                    double ageFactor = 720;
                    if (!Blobs[b].Sclerote && !Blobs[b].Dead && ageFactor / Blobs[b].Age > r.NextDouble())
                    {
                        Blobs[b].Starve();
                        Blobs[b].Feed();
                        
                        Blobs[b].Learn();
                        Blobs[b].MoveAndGrow();
                        
                    }

                    try {this.UpdateBlobState(Blobs[b]);} catch {}

                }

                foreach(Spore spore in this.Spores)
                {
                    if (spore.Move(spore.Position.Voisins))
                    {
                        this.Spores.Remove(spore);
                        this.AddBlob(spore.Position,spore.Sexe);
                    }
                }
                
            }
        }
        /*
            Appelé lorsque on clique avec la souris sur une case avec l'élément "Nourriture" sélectionné. Pose de la nourriture dans la case
        */
        public void AddFood(Case pos, int qte, double concentration, double ratio)
        {
            Food food = new Food(pos,-100,qte,concentration,ratio);
            this.AddAgent(food,pos);
        }
        /*
            Appelé lorsque on clique avec la souris sur une case avec l'élément "Sel" sélectionné. Pose du sel dans la case
        */
        public void AddSalt(Case pos)
        {
            pos.AddSalt();
        }
        /*
            Appelé lorsque on clique avec la souris sur une case avec l'élément "Lumière" sélectionné. Illumine la case
        */
        public void AddLight(Case pos)
        {
            Emitter.Emitter light = new Emitter.Emitter("Light" , pos , 10 , -1);
            this.AddAgent(light,pos);
        }
        /*
            Appelé lorsque on clique avec la souris sur une case avec l'élément "Blob" sélectionné. Génère un blob avec deux veines collées dont une sur la case
        */
        public void AddBlob(Case pos, int sexe)
        {
            List<Case> voisins = this.Voisinage(pos , 1);

            Random r = new Random();
            Case pos2 = voisins[r.Next(voisins.Count)];

            if (pos.Veine == null && pos2.Veine == null)
            {
                Blob.Blob blob = new Blob.Blob(this, this.Blobs.Count, pos, pos2, sexe);
                this.Blobs.Add(blob);
            }
        }
        /*
            Supprime le blob de la simulation
        */
        public void RemoveBlob(Blob.Blob blob)
        {
            this.Blobs.Remove(blob);
        }


    }
}