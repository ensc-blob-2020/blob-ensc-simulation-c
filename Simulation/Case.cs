using System.Collections.Generic;
using Blob;
using System.Text;
using System;

namespace World {
    [Serializable]
    class Case {
        // Index horizontal de la case, compris entre 0 et la taille de la grille moins un (24 par défaut).
        public int X {get; set;}
        // Index vertical de la case, compris entre 0 et la taille de la grille moins un (24 par défaut).
        public int Y {get; set;}
        // Attractivité de la case en fonction des champs d'émission de tous les émetteurs.
        public double ValeurDeChamp {get; set;}
        // Liste des émetteurs présents sur la case
        public List<Emitter.Emitter> Agents {get; set;}
        // Liste des spores présents sur la case
        List<Spore> Spores {get; set;}
        // Liste des cases voisines de la case. 
        public List<Case> Voisins {get; set;}
        // Veine présente sur la case, null si il n'y en a pas
        public Vein Veine {get; set;}
        // Vrai si la case contient du mucus
        public bool Mucus {get; set;}
        // Vrai si la case contient du sel
        public bool Salt {get; set;}
        // Référence à la grille de simulation
        public Grid World;

        public Case(int x,int y, Grid grille)
        {
            this.X = x;
            this.Y = y;

            this.ValeurDeChamp = 0;
            this.Agents = new List<Emitter.Emitter>();
            this.Spores = new List<Spore>();
            this.Voisins = new List<Case>();
            this.Veine = null;
            this.Mucus = false;
            this.Salt = false;
            this.World = grille;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();

            str.AppendFormat("Case ({0},{1}) :\n",X,Y);
            if (Salt) str.Append("\tSalée\n");
            if (Mucus) str.Append("\tMucusée\n");
            str.AppendFormat("\tSpores : {0}",Spores.Count);
            str.Append("Emetteurs :\n");
            foreach(Emitter.Emitter emitter in Agents)
            {
                str.Append(emitter);
                str.Append("---\n");
            }
            
            return str.ToString();
        }
        /*
            Remet à jour la valeur de champ, pour la recalculer.
        */
        public void ResetField()
        {
            this.ValeurDeChamp = 0;
        }
        /*
            Retourne vrai si un émetteur du type spécifié est présent dans la liste des émetteurs
        */
        public bool IsIn(string nom)
        {
            foreach(var emitter in this.Agents)
            {
                if (emitter.Name == nom) {
                    return(true);
                }
            }
            return(false);
        }
        /*
            Ajoute du sel à la case
        */
        public void AddSalt()
        {
            this.Salt = true;
        }
        /*
            Ajoute un agent sur la case.
        */
        public void AddAgent(Emitter.Emitter agent)
        {
            this.Agents.Add(agent);
        }
        /*
            Supprime la veine sur la case si il y en avait une
        */
        public void RemoveVein()
        {
            this.Veine = null;
        }
        /*
            Supprime une certaine spore dans la liste des spores
        */
        public void RemoveSpore(Spore spore)
        {
            this.Spores.Remove(spore);
        }
        /*
            Ajoute une spore sur la case
        */
        public void AddSpore(Spore spore)
        {
            this.Spores.Add(spore);
        }
        /*
            Prend en compte un champ dans valeurDeChamp
        */
        public void MergeField(double value)
        {
            this.ValeurDeChamp += value;
        }
        /*
            Retourne vrai si il n'y a pas de veine sur la case et si il y a une spore qui peut se reproduire avec la dernière ajoutée
        */
        public bool TimeToken()
        {
            if (this.Spores.Count > 1 && this.Veine == null)
            {
                int sex = this.Spores[Spores.Count - 1].Sexe;
                for (int i = 1 ; i < this.Spores.Count ; i++)
                {
                    if (sex != this.Spores[i].Sexe)
                    {
                        return(true);
                    }
                }
            }
            return(false);
        }


    }
}