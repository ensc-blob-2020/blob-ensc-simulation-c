using World;
using System;
using System.Text;

namespace Emitter {
    [Serializable]
    class Emitter {
        // Type d'émetteur. Sont utilisés comme noms dans le code : "Food", "Light", 
        public string Name {get; set;}
        // Puissance d'émission de base du signal d'émission
        public double Power {get; set;}
        // Nombre négatif, correspond à l'estomption du signal d'émission par case.
        public double Decay {get; set;}
        public Case Position {get; set;}
        // Distance maximale à laquelle l'émetteur peut être reçu par un blob
        public int Distance {get; set;}

        /*
            Sont considérés comme émetteurs les éléments de la grille que le blob suit ou fuit.
            Nourriture ; Lumière ; Sel ; Mucus
        */
        public Emitter(string name, Case position, double power, double decay)
        {
            this.Name = name;
            this.Power = power;
            this.Decay = decay;
            this.Position = position;
            this.Distance = (int) Math.Abs(power/decay);
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append(Name);
            str.Append(" :\n");
            str.AppendFormat("\tPuissance d'émission : {0}\n",Power);
            str.AppendFormat("\tEstomption par case : {0}\n",Decay);

            return(str.ToString());
        }

    }



    [Serializable]
    class Food : Emitter {
        // Quantité de nourriture.
        int quantité {get; set;}
        // Concentration en nourriture
        public double concentration {get; set;}
        // proteine / (proteine + glucide)
        public double ratio {get; set;}
        
        /*
            Food est un type d'émetteur.
        */
        public Food(
            Case pos,
            float decay,
            int qte,
            double concentration,
            double ratio) : base("Food", pos, 0, decay)
        {

            this.Power = ratio == 1 ? 0.00001 * concentration : concentration / (1 - ratio);

            this.quantité = qte;
            this.concentration = concentration;
            this.ratio = ratio;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder(base.ToString());
            str.AppendFormat("\tQuantité : {0}\n",quantité);
            str.AppendFormat("\tConcentration relative : {0}\n",concentration);
            str.AppendFormat("\tRatio protéines/glucides : {0}\n",ratio);

            return(str.ToString());
        }
        /*
            Fonction appellée lorsque le blob mange la nourriture.
        */
        public double Eat()
        {
            this.quantité -= 1;
            if (this.quantité > 0)
            {
                this.Position.World.RemoveAgent(this,this.Position);
            }
            return(this.ratio);
        }


    }

}